pragma solidity >=0.6.2 <0.8.0;
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";
import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/access/Ownable.sol";

contract Coin is  Ownable {
    using SafeMath for uint256;
    
    event Transfer(
        address indexed _from,
        address indexed _to,
        string _symbol,
        uint256 _value
    );

    event Approval(
        address indexed _owner,
        address indexed _spender,
        string _symbol,
        uint256 _value
    );

    mapping (address => uint256)  _balances;

    mapping (address => mapping (address => uint256)) public _allowances;

    uint256 public _totalSupply;
    string public _name;
    string public _symbol;
    uint8 public _decimals;
    uint256 public _APR;
    struct User { 
      
        uint256 flexible;
        uint256 week;
        uint256 startFlexible;
        
        uint256 rewards;
        uint256 rewardFlexible;
        
        }
    
    mapping (address =>  User) public lpToken;
    address[] public stakeholders;
    
    mapping (address =>  User) public info;
    
    constructor (uint256 APR) {
        _name = "LPToken";
        _symbol = "LP";
        _decimals = 18;
        _APR = APR;
    }
    //----------------------------------------------Stake-------------------------------------
    
    
    function isStakeholder(address _address) public view returns(bool, uint256)
    {
       for (uint256 s = 0; s < stakeholders.length; s += 1){
           if (_address == stakeholders[s]) return (true, s);
       }
       return (false, 0);
    }
    function addStakeholder(address _stakeholder) private
    {
       (bool _isStakeholder ,) = isStakeholder(_stakeholder);
       if(!_isStakeholder) stakeholders.push(_stakeholder);
    }
   function removeStakeholder(address _stakeholder)
       private
    {
       (bool _isStakeholder,uint256 s ) = isStakeholder(_stakeholder);
       if(_isStakeholder){
           stakeholders[s] = stakeholders[stakeholders.length - 1];
           stakeholders.pop();
       }
    }

    function totalStakes()
       public
       view
       returns(uint256)
   {
       uint256 _totalStakes = 0;
       
           
           for (uint256 s = 0; s < stakeholders.length; s += 1){
               _totalStakes = _totalStakes.add(info[stakeholders[s]].flexible);
               _totalStakes = _totalStakes.add(info[stakeholders[s]].week);
           }   
           
       return _totalStakes;
   
   }
   
    
    
    
    
     function createStakeFlexible(uint256 _stake )
       public
    {
       _burn(msg.sender, _stake);
       if(info[msg.sender].flexible == 0 ) {
           addStakeholder(msg.sender);
           
       }   
           info[msg.sender].flexible = info[msg.sender].flexible.add(_stake);
           info[msg.sender].startFlexible = block.timestamp;
       
       
    }
   
    
    function removeStakeFlexible(uint256 _stake )
      public
  {
      info[msg.sender].flexible = info[msg.sender].flexible.sub(_stake);
      if(info[msg.sender].flexible == 0) removeStakeholder(msg.sender);
       
    
      _mint(msg.sender, _stake);
  }
    
  function rewardOf(address _stakeholder  )
      public
      view
      returns( uint256)
  {
      return (info[_stakeholder].rewardFlexible );
  }


  function totalRewards()
      public
      view
      returns(uint256)
  {
      uint256 _totalRewards = 0;
      
          for (uint256 s = 0; s < stakeholders.length; s += 1){
              _totalRewards = _totalRewards.add(info[stakeholders[s]].rewardFlexible);
             
              
          }    
      
      return _totalRewards;
  }
  function calculateRewardFlexible(address _stakeholder )
      public
      view
      returns(uint256)
  {    
       uint256 x = info[_stakeholder].flexible/100 * (_APR/365);
      return x ;
  }
    
  function distributeRewards()
      public
      onlyOwner
  {
      
      for (uint256 s = 0; s < stakeholders.length; s += 1){
          address stakeholder = stakeholders[s];
          if (info[stakeholder].flexible != 0 && info[stakeholder].startFlexible <= block.timestamp - 1 seconds) {
            uint256 reward = calculateRewardFlexible(stakeholder);
            
            info[stakeholder].rewardFlexible = info[stakeholder].rewardFlexible.add(reward);
            info[stakeholder].startFlexible = block.timestamp;
            }
         
      }
           
      
  }
   
  function withdrawReward()
      public
  {
      uint256 reward = info[msg.sender].rewardFlexible;
      info[msg.sender].rewardFlexible = 0;
      
      _mint(msg.sender, reward);
  }
   
   
    //--------------------------------------------------Coin---------------------------------------------------
    function totalSupply() public view  returns (uint256) {
        return _totalSupply;
    }
    function balanceOf(address account) public view  returns (uint256) {
        return _balances[account];
    }
    function transfer(address recipient, uint256 amount) public   returns (bool) {
        
        _balances[msg.sender] -= amount;
        _balances[recipient] += amount;

        emit Transfer(msg.sender, recipient, _symbol, amount);
        
        return true;
    }
    function allowance(address owner, address spender) public view   returns (uint256) {
        return _allowances[owner][spender];
    }
    function approve(address spender, uint256 amount) public   returns (bool) {
        _allowances[msg.sender][spender] = amount;

        emit Approval(msg.sender, spender, _symbol, amount);
        
        return true;
    }
    
    function transferFrom(address sender, address recipient, uint256 amount) public   returns (bool) {
        
        _balances[sender] -= amount;
        _balances[recipient] += amount;
        approve(recipient, _allowances[sender][recipient]-amount);
        //_allowances[sender][recipient] = _allowances[sender][recipient]-amount;
    
        return true;
    
    }
    function _mint(address account, uint256 amount) public  {
        require(account != address(0), "ERC20: mint to the zero address");

        

        _totalSupply += amount;
        _balances[account] += amount;
    }
    function _burn(address account, uint256 amount) public virtual {
        require(account != address(0), "ERC20: burn from the zero address");

        

        _balances[account] = _balances[account].sub(amount, "ERC20: burn amount exceeds balance");
        _totalSupply = _totalSupply.sub(amount);
        
    }
    
}
