import React, {Component} from 'react';
import{ COIN_ABI, COIN_ADDRESS } from './config'
import Web3 from 'web3'

class App extends Component {
  componentWillMount() {
    this.loadBlockchainData()
  }
  async loadBlockchainData() {
    const web3 = new Web3("http://localhost:7545")
    const accounts = await web3.eth.getAccounts()
    
    this.setState({ account: accounts[0] })
    this.setState({ account2: accounts[1] })
    
    const Coin = new web3.eth.Contract(COIN_ABI, COIN_ADDRESS)
    this.setState({ Coin })
    console.log(Coin)
    Coin.methods.balanceOf(accounts[0])
    .call().then(result1=> this.setState({balances : result1}))
    Coin.methods.balanceOf( accounts[1])
    .call().then(result1=> this.setState({balances2 : result1}))
    Coin.methods.totalSupply()
    .call().then(result1=> this.setState({total : result1}))

  }  
  
  
  
  constructor(props) {
    super(props)
    
    
    this.state = {
      account: '',
      account2: '',
      balances: '',
      balances2: '',
      total:'',
      mintAddr: '',
      mintValue: 0,
      recipientAddr: '',
      tranValue:0,
      tranferFrom:''

    }

    this.handleChange5 = this.handleChange5.bind(this);
    this.handleChange4 = this.handleChange4.bind(this);
    this.handleChange3 = this.handleChange3.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.mint = this.mint.bind(this)
    this.transfer = this.transfer.bind(this)

  }

  mint(event){
    this.state.Coin.methods._mint(this.state.mintAddr,this.state.mintValue)
    .send({from:this.state.mintAddr})
    alert('mint: ' + this.state.mintValue);
    event.preventDefault();
  }

  transfer(event){

    console.log(this.state.tranferFrom)
    this.state.Coin.methods.transfer(this.state.recipientAddr,this.state.tranValue)
    .send({from:this.state.tranferFrom})
    alert('transfer to: ' + this.state.recipientAddr +' from: ' +this.state.tranferFrom+' amount: '+this.state.tranValue);
    event.preventDefault();

  }


  

  
   
  handleChange(event) {
    this.setState({mintValue: event.target.value});
  }
  handleChange2(event) {
    this.setState({mintAddr: event.target.value});
  }
  
  handleChange3(event) {
    this.setState({tranValue: event.target.value});
  }
  handleChange4(event) {
    this.setState({recipientAddr: event.target.value});
  }
  handleChange5(event) {
    this.setState({tranferFrom: event.target.value});
  }
  handleSubmit(event) {
    
    alert('mint: ' + this.state.value);
    event.preventDefault();
  }




  render(){
    return(
      <div>
        
        <p>Account1: {this.state.account}</p>
        <p>balances: {this.state.balances}</p>
        <p>Account2: {this.state.account2}</p>
        <p>balances: {this.state.balances2}</p>
        
        <p>total: {this.state.total}</p>
        
        

        <form onSubmit={this.mint}>
          <label>
              Amount to mint:
              <input type="number" 
               onChange={this.handleChange} /><p></p>
               
                Address to mint:
              <input type="text" 
               onChange={this.handleChange2} />
          </label><p></p>
          < input type="submit" value="Submit" />
        </form>
        <form onSubmit={this.transfer}>
          <label>
              Transfer from:
              <input type="text" 
               onChange={this.handleChange5} /><p></p>
              Amount to transfer:
              <input type="number" 
               onChange={this.handleChange3} /><p></p>
                Reciept address:
              <input type="text" 
               onChange={this.handleChange4} />
          </label><p></p>
          < input type="submit" value="Submit" />
        </form>
        
      </div>
    );
  }

}
 
export default App;
